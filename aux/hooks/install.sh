#!/bin/sh -eu
for hook in pre-commit ; do
    src="aux/hooks/${hook}"
    dest=".git/hooks/${hook}"
    [ -x "${src}" ] && ln -sf ../../"${src}" "${dest}"
done
