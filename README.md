# mbug

## Rationale

This program makes working with old Debian bugs much more pleasant.
Sometimes I want to have personal bug-squashing party, so you open
[https://bugs.debian.org/kaction@gnu.org] in web-browser, and get nice
list of bugs that require my attention.

Browsing is great, but when I want to do something with those bugs --
close, add tags, discuss, I have to copy bug number, launch mail client
in another terminal and compose email to [mailto:nnn@bugs.debian.org].

Should I need to quote some previous message, more distracting actions
are to be done -- download whole bug as mbox, incorporate that mbox into
my mail system (I prefer MH), and only after that real work could start.

DebBugs has also email interface -- you can request sending to you bugs
you want, but reply is also optimized for reading, not for manipulating.
Honestly, it looks like corresponding web page, filtered by `lynx`.

## Solution

~~~~
mbug [--into <folder>] <query>
~~~~

It will download mbox of every bug, listed at
`https://bugs.debian.org/<query>` and create `+<folder>` MH folder. In
that folder subfolder for every bug is created. By default, `<folder>`
is `debbugs`.

Folder `+<folder>` is volatile, every new invocation of `mbox` will
overwrite its previous content. This way you can focus on bugs you are
interested in right now.
