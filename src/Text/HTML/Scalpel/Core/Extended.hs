module Text.HTML.Scalpel.Core.Extended
  ( module Text.HTML.Scalpel.Core
  , guardPos
  ) where
import Control.Monad          (guard)
import Text.HTML.Scalpel.Core
import Text.StringLike        (StringLike)

-- | Skip chroots at index, not satisfying predicate
guardPos :: (Ord a, StringLike a) => (Int -> Bool) -> Scraper a ()
guardPos p = guard =<< (fmap p position)
