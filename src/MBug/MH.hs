module MBug.MH (resolve) where
import MBug.Data.FolderMH (FolderMH, showFolderMH)
import System.Process     (readProcess)

-- | Thin wrapper around mhpath(1) to get filepath to directory,
-- corresponding to MH folder.
resolve :: FolderMH -> IO FilePath
resolve folder =
  takeWhile (/= '\n') <$> readProcess "/usr/bin/mh/mhpath" [showFolderMH folder] ""
