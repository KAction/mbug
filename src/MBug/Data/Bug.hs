module MBug.Data.Bug (Bug(..)) where
import Data.Text (Text)

-- | Brief information about bug, that is extracted from request page
data Bug = Bug {
  _number   :: Int,  -- ^ bug number.
  _severity :: Text, -- ^ bug severity (FIXME: should be enumeration)
  _package  :: Text, -- ^ package to which bug assigned to
  _subject  :: Text  -- ^ bug subject
} deriving Show
