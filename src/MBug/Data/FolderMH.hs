{-# LANGUAGE LambdaCase #-}
module MBug.Data.FolderMH
  ( FolderMH(..)
  , readFolderMH
  , showFolderMH
  ) where

import           Data.Text (Text)
import qualified Data.Text as T

data FolderMH
  = Absolute Text
  | Relative Text

-- | Convert 'FolderMH' value into String. The output is formatted like
-- programs from mh(7) suite would expect, and does not look like
-- automatically generated 'Show' instance.
--
-- 'String' datatype is used, since it is part of "Options.Applicative"
-- module API.
showFolderMH :: FolderMH -> String
showFolderMH = \case
  Absolute txt -> "+" ++ T.unpack txt
  Relative txt -> "@" ++ T.unpack txt

-- | Parse 'FolderMH' value from String. That string must begin with
-- either @+@ character (absolute folder) or @\@@ (relative folder)
-- for parsing to succeed.
--
-- 'String' datatype is used, since it is part of "Options.Applicative"
-- module API.
readFolderMH :: String -> Maybe FolderMH
readFolderMH = \case
  '+' : str -> Just $ Absolute (T.pack str)
  '@' : str -> Just $ Relative (T.pack str)
  _         -> Nothing
